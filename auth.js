const jwt =require ("jsonwebtoken");
// Used in the algorithm for encrypting our data which makes it difficult to decode the information with the defined secret keyword
const secret = "EcommerceAPI";

// [ SECTION ] JSON Web Tokens
	// JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server

// Token Creation
module.exports.createAccessToken =(user) =>{
    // The data will be received from the registration form
	// When the user logs in, a token will be created with user's information
	// payload
    const data ={
        id :user._id,
        email : user.email,
        isAdmin : user.isAdmin
    };
    return jwt.sign(data,secret,{});
}


module.exports.verify =(req,res,next)=>{
    // The token is retrieved from the request header
	// This can be provided in postman under
		// Authorization > Bearer Token
    let token =req.headers.authorization
    console.log(token);
    if(typeof token !== "undefined"){
        token =token.slice(7,token.length);
        console.log(token);
        return jwt.verify(token,secret,(err,data) =>{
            if (err){
                return res.send({auth : "failed"});
            } else {
            	// Allows the application to proceed with the next middleware function/callback function in the route
                next()
            }
        })
    }else {
        return res.send({ auth : "failed"});
    }
}

module.exports.decode =(token) =>{
    if(typeof token !== "undefined"){
        token =token.slice(7,token.length);
        return jwt.verify(token,secret,(err,data) =>{
            if (err){
                return null;
            } else {
                // The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
                return jwt.decode(token, {complete : true}).payload;
            };
        })
    } else {
    return null
    }
}
