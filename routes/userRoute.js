const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController")
const auth = require("../auth")

router.post("/register",(req,res) =>{
    userController.registerUser(req.body).then(resultFromController => res.send(
        resultFromController
    ));
});

router.post("/login", (req,res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/checkout", auth.verify, (req,res) => {
    const data = {
        order : req.body,
        userId: auth.decode(req.headers.authorization).id,

        isAdmin : auth.decode(req.headers.authorization).isAdmin
    };
    userController.checkout(data).then(resultFromController => res.send(resultFromController));
});

router.get("/:userId/userDetails", auth.verify, (req,res) => {
    userController.getUserDetails(req.params).then(resultFromController => res.send(resultFromController));
});

router.get("/orders", auth.verify, (req,res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;

    userController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
});

router.get("/myOrders", auth.verify, (req,res) => {
    let userId = auth.decode(req.headers.authorization).id;

    userController.getMyOrders(userId).then(resultFromController => res.send(resultFromController));
});

router.patch("/:userId/setAsAdmin", auth.verify, (req,res) => {
    let isAdmin = auth.decode(req.headers.authorization).isAdmin;
    userController.setAsAdmin(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

module.exports = router;