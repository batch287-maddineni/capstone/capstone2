const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth =require("../auth");

module.exports.addProduct =(data) =>{
    if (data.isAdmin){
    let newProduct = new Product({
        name: data.product.name,
        description: data.product.description,
        price: data.product.price
    }) 
    // Saves the created object to our database
    return newProduct.save().then((product,error)=>{
        if (error){
            return false
        }else {
            return true
        }
    })
    }
    
    let message = Promise.resolve("User must be an Admin to add a product");
    return message.then((value)=>{
        return value;
    })

}

module.exports.getAllProducts =() =>{
    return Product.find({}).then(result =>{
        return result;
    })
}

module.exports.getActiveProducts =() =>{
    return Product.find({isActive: true}).then(result =>{
        return result;
    })
}

module.exports.getProduct =(reqParams) =>{
    return Product.findById(reqParams.productId).then(result =>{
        return result;
    })
}

module.exports.updateProduct =(reqParams,data) =>{
	if(data.isAdmin){
		    let updatedProduct ={
        name: data.product.name,
        description: data.product.description,
        price: data.product.price
    }
    return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((course, error)=>{
            if(error){
                return false;
            }else{
                return true;
            }
        })
	}

    let message = Promise.resolve("User must be an Admin to update a product");
    return message.then((value)=>{
        return value;
    })
}

module.exports.archiveProduct =(reqParams,isAdmin) =>{
	if(isAdmin){
		    let archivedProduct ={
		    	isActive : false
    }
    return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then((course, error)=>{
            if(error){
                return false;
            }else{
                return true;
            }
        })
	}

    let message = Promise.resolve("User must be an Admin to archive a product");
    return message.then((value)=>{
        return value;
    })
}

module.exports.activateProduct =(reqParams,isAdmin) =>{
	if(isAdmin){
		    let archivedProduct ={
		    	isActive : true
    }
    return Product.findByIdAndUpdate(reqParams.productId,archivedProduct).then((course, error)=>{
            if(error){
                return false;
            }else{
                return true;
            }
        })
	}

    let message = Promise.resolve("User must be an Admin to activate a product");
    return message.then((value)=>{
        return value;
    })
}