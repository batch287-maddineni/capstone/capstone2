const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth =require("../auth");

module.exports.registerUser = (reqBody) =>{
    let newUser = new User({
        email : reqBody.email,
        password : bcrypt.hashSync(reqBody.password,10)
    })
    return newUser.save().then((user,error) =>{
        if(error){
            return false
        } else {
            return true
        }
    })
}

module.exports.loginUser = (reqBody) => {

    // .findOne() will look for the first document that matches
    return User.findOne({ email: reqBody.email }).then(result => {

        console.log(result);

        // Email doesn't exist
        if(result == null){

            return false;

        } else {

            // We now know that the email is correct, is password correct also?
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            console.log(isPasswordCorrect);
            console.log(result);

            // Correct password
            if(isPasswordCorrect){

                return { access: auth.createAccessToken(result) }

            // Password incorrect   
            } else {

                return false
                
            };

        };
    });
};

module.exports.checkout = async (data) =>{

    let prices = 0;
    for (let i = 0; i < data.order.length; i++) {
        let subPrice =await Product.findById(data.order[i].productId).then(result =>{return result.price;});
        prices += subPrice * data.order[i].quantity;
    }

    if(!data.isAdmin){
        let newOrder = new Order({
            userId : data.userId,
            products : data.order,
            totalAmount : prices
        });

        console.log(newOrder);

        return newOrder.save().then((order,error) =>{
            if(error){
                return false
            }
            else {
                return true
            }
        })
    }
}

module.exports.getUserDetails =(reqParams) =>{
    return User.findById(reqParams.userId).then(result =>{
        result = {
            email: result.email,
            isAdmin: result.isAdmin,
            password: "hidden for security reasons"
        }
        return result;
    })
}

module.exports.setAsAdmin =(reqParams, isAdmin) =>{
    if(isAdmin){
        return User.findById(reqParams.userId).then(result =>{
        updatedUser = {
            isAdmin: true
        }
        return User.findByIdAndUpdate(reqParams.userId,updatedUser).then((product, error)=>{
            if(error){
                return false;
            }else{
                return true;
            }
        });
    })
    }
    else{
        return "user must be admin to do this"
    }
}

module.exports.getAllOrders = (isAdmin) =>{
    if (isAdmin) {
        Order.find({}).then(result =>{
            return result;
    })
    }

}

module.exports.getMyOrders = (uId) =>{
    return Order.find({userId : uId}).then(result =>{
        return result;
    })}