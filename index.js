const express = require("express");
const mongoose = require("mongoose");
const userRoutes = require("./routes/userRoute.js");
const productRoutes = require("./routes/productRoute.js");
const orderRoutes = require("./routes/orderRoute.js");

// Allows us to control the app's Cross Origin Resource Sharing settings
// Allow our backend application to be available to our frontend application
const cors = require("cors")
// Create an "app" variable that stores the result of the express function
const app = express();

// Connect to Mongodb Database
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.rp37zkh.mongodb.net/capstone-2",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

mongoose.connection.once("open", () => console.log('Now connected in the cloud.'))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Will use the defined port number for the application whenever the variable is available or will use port 4000 if none is defined
// Doing this syntax, it will allow flexibility when using the application locally or as a hosted application.
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});

app.use("/users",userRoutes);
app.use("/products",productRoutes);
app.use("/orders",orderRoutes);